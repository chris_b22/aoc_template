const settings = {
    folderForEveryDay: false,
    filenameSchema: 'day_{day}_{part}',     // use {day} and {part} to create a file for each day and part
    dataFilenameSchema: "data-{day}",             // use {day} to create a file for each day
    dataLocation: 'root',                   // 'root' or 'day' - where to save the data(folder)
    createDataFolder: true,                 // if true, the data will be saved in a "data" folder at root or day.
    year: 2024,                             // the year of the event
}

module.exports = settings;