const fs = require( 'fs' );
const fetch = require( 'node-fetch' );
const settings = require( './settings' );




if ( dataLocation === 'root' ) dayFolder = ''
if ( dataFolder ) fs.mkdirSync( path.join( dayFolder, dataFolder ), { recursive: true } );
fs.writeFileSync( path.join( dayFolder, dataFolder, `${dataFilename}.txt` ), '' );
fs.writeFileSync( path.join( dayFolder, dataFolder, `${dataFilename}-test.txt` ), '' );


async function getDayInputData( day, testData = false ) {
    const {
        folderForEveryDay,
        dataFilenameSchema,
        createDataFolder,
        dataLocation
    } = settings;

    const day = day.toString().padStart( 2, '0' );
    let dayFolder = folderForEveryDay ? day : '';
    const dataFolder = createDataFolder ? 'data' : '';
    const dataFilename = dataFilenameSchema.replace( '{day}', day );

    let path = path.join( dayFolder, dataFolder, `${dataFilename}-test.txt` )

    // let filename = testData ? `data-${day}-test.txt` : `data-${day}.txt`
    // let pathFileToData = './'
    // if ( dataLocation === 'root' && dayFolder ) pathFileToData = '../';
    // if ( createDataFolder ) pathFileToData += 'data/';
    // pathFileToData += filename;

    // try {
    //     const data = fs.readFileSync( pathFileToData, { encoding: 'utf-8' } );
    // } catch ( error ) {
    //     if ( testData ) {
    //         console.error( 'Test data not found. Fetching input data.' );
    //     }
    // }

    /**
     * Fetches data from the given URL.
     * 
     * @param {string} url - The URL to fetch data from.
     * @returns {Promise<string>} The fetched data as a string.
    */
    async function fetchData( year, day ) {



        if ( dataLocation === 'root' ) dayFolder = ''
        if ( dataFolder ) fs.mkdirSync( path.join( dayFolder, dataFolder ), { recursive: true } );

        const dataFilename = dataFilenameSchema.replace( '{day}', day );

        const url = `https://adventofcode.com/${year}/day/${day}/input`;
        try {
            if ( !process.env.AOCCOOKIE ) {
                throw new Error( 'Cookie not set. Set it in .env file.' );
            }
            const response = await fetch( url, {
                headers: {
                    'User-Agent': 'Mozilla/5.0',
                    'Cookie': 'session=' + process.env.AOCCOOKIE
                }
            } );
            if ( !response.ok ) {
                throw new Error( `HTTP error! status: ${response.status}` );
            }
            const data = await response.text();
            return data;
        } catch ( error ) {
            console.error( 'Error fetching data:', error );
            throw error;
        }
    }

    // Usage example
    fetchData( 'https://adventofcode.com/2024/day/10/input' )
        .then( data => console.log( data ) )
        .catch( error => console.error( error ) );

