const fs = require( 'fs' );
const path = require( 'path' );
const settings = require( './settings' );
const templateContent = fs.readFileSync( path.join( __dirname, 'template.js' ), 'utf8' );

function createFoldersWithFiles( settings ) {
    const {
        folderForEveryDay,
        filenameSchema,
        dataFilenameSchema,
        createDataFolder,
        dataLocation
    } = settings;

    if ( !filenameSchema.includes( '{day}' ) || !filenameSchema.includes( '{day}' ) ) {
        console.error( 'filenameSchema must include {day} and {part}' );
        return;
    }
    for ( let i = 1; i <= 25; i++ ) {
        const day = i.toString().padStart( 2, '0' );
        let dayFolder = folderForEveryDay ? day : '';
        const dataFolder = createDataFolder ? 'data' : '';
        const filename = filenameSchema.replace( '{day}', day );
        const f1Path = path.join( dayFolder, filename.replace( '{part}', '1' ) + '.js' );
        const f2Path = path.join( dayFolder, filename.replace( '{part}', '2' ) + '.js' );
        const dataFilename = dataFilenameSchema.replace( '{day}', day );

        if ( dayFolder ) fs.mkdirSync( day, { recursive: true } );

        // let pathFileToData = './'
        // if ( dataLocation === 'root' && dayFolder ) pathFileToData = '../';
        // if ( dataFolder ) pathFileToData += 'data/';
        // pathFileToData += 'data-test.txt'
        // const file = templateContent.replace( '{datapath}', pathFileToData );

        if ( !fs.existsSync( f1Path ) ) fs.writeFileSync( f1Path, file );
        if ( !fs.existsSync( f2Path ) ) fs.writeFileSync( f2Path, file );

        if ( dataLocation === 'root' ) dayFolder = ''
        if ( dataFolder ) fs.mkdirSync( path.join( dayFolder, dataFolder ), { recursive: true } );
        // fs.writeFileSync( path.join( dayFolder, dataFolder, `${dataFilename}.txt` ), '' );
        fs.writeFileSync( path.join( dayFolder, dataFolder, `${dataFilename}-test.txt` ), '' );
    }
    console.log( 'Files created.' );
}

function getPathFileToData( settings, day ) {
    const { folderForEveryDay, dataLocation } = settings;
    let dayFolder = folderForEveryDay ? day : '';
    let pathFileToData = './';
    if ( dataLocation === 'root' && dayFolder ) pathFileToData = '../';
    return pathFileToData;
}

createFoldersWithFiles( settings );