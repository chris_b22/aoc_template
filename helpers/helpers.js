// Read a file:
const fs = require( 'fs' );
const data = fs.readFileSync( './data-test.txt', { encoding: 'utf-8' } );
// console.log( 'data :>> ', data );



// split the data into rows:    
let rows = data.split( '\r\n' );

// split at empty lines:
let blocks = data.split( '\r\n\r\n' );


// All values of a two dimensional array:
for ( let x = 0; x < map.length; x++ ) {
    for ( let y = 0; y < map[ x ].length; y++ ) {
    }
}

/**
 * Iterates over a 2D array and applies a callback function to each element.
 * 
 * @param {Array<Array<any>>} map - The 2D array to iterate over.
 * @param {function(number, number): void} callback - The callback function to apply to each element.
 */
const map2D = ( map, callback ) => {
    for ( let x = 0; x < map.length; x++ ) {
        for ( let y = 0; y < map[ x ].length; y++ ) {
            callback( x, y );
        }
    }
}

/**
 * Gets the neighbouring elements of a given position in a 2D array.
 * 
 * @param {number} x - The x-coordinate of the element.
 * @param {number} y - The y-coordinate of the element.
 * @param {Array<Array<any>>} map - The 2D array.
 * @returns {Array<any>} The neighbouring elements.
 */
const getAllNeighbours = ( x, y, map ) => {
    const neighbours = [];
    for ( let dx = -1; dx <= 1; dx++ ) {
        for ( let dy = -1; dy <= 1; dy++ ) {
            if ( dx === 0 && dy === 0 ) continue;
            let nx = x + dx;
            let ny = y + dy;
            if ( nx >= 0 && nx < map.length && ny >= 0 && ny < map[ 0 ].length ) {
                neighbours.push( map[ nx ][ ny ] );
            }
        }
    }
    return neighbours;
}

/**
 * Gets the neighbouring elements of a given position in a 2D array, excluding diagonal neighbours.
 * 
 * @param {number} x - The x-coordinate of the element.
 * @param {number} y - The y-coordinate of the element.
 * @param {Array<Array<any>>} map - The 2D array.
 * @returns {Array<any>} The neighbouring elements.
 */
const getOrthogonalNeighbours = ( x, y, map ) => {
    const neighbours = [];
    const directions = [
        { dx: -1, dy: 0 }, // left
        { dx: 1, dy: 0 },  // right
        { dx: 0, dy: -1 }, // up
        { dx: 0, dy: 1 }   // down
    ];

    for ( const { dx, dy } of directions ) {
        let nx = x + dx;
        let ny = y + dy;
        if ( nx >= 0 && nx < map.length && ny >= 0 && ny < map[ 0 ].length ) {
            neighbours.push( map[ nx ][ ny ] );
        }
    }
    return neighbours;
}


/**
 * Checks if the given coordinates are within the boundaries of the 2D array.
 * 
 * @param {number} x - The x-coordinate.
 * @param {number} y - The y-coordinate.
 * @param {Array<Array<any>>} map - The 2D array.
 * @returns {boolean} True if the coordinates are within the boundaries, false otherwise.
 */
function isWithinMapBounds( x, y, map ) {
    return x >= 0 && y >= 0 && x < map.length && y < map[ 0 ].length;
}

module.exports = {
    map2D,
    getOrthogonalNeighbours,
    getAllNeighbours,
    isWithinMapBounds
};


